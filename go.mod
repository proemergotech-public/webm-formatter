module gitlab.com/proemergotech/webm-formatter

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/pkg/errors v0.8.1
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980 // indirect
)
