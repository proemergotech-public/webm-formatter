# Webm formatter

Ebml parser/formatter with webm support.

# EBML
Ebml is an xml-like language for binary, meaning nodes form a document tree. Leaf nodes can be one of:
- [IntNode](ebml/int_node.go)
- [UintNode](ebml/uint_node.go)
- [FloatNode](ebml/float_node.go)
- [StringNode](ebml/string_node.go)
  - ASCII string only
- [UTF8Node](ebml/utf8_node.go)
- [DateNode](ebml/date_node.go)
- [BinaryNode](ebml/binary_node.go)

Non-leaf nodes are called [MasterNode](ebml/master_node.go).

## EBML Node

A single node in ebml has the following structure: `<ElementID><Length><Data>`

Example: `0x42 0x82 0x84 0x77 0x65 0x62 0x6d`
- ElementID: `0x42 0x82` - identifies the semantic type of the node element, in this case `0x42 0x82` means a DocType element
- Length: `0x84` = `10000100` in binary - the length of the data, in this case 4 (see [VINT](#VINT) for details), can be unknown
                                  for master nodes (in case of streaming where length is not known at the start of the
                                  stream)
- Data: `0x77 0x65 0x62 0x6d` = `webm` - the content of the element, in this case a string (because DocType is a StringNode
                                   element)
                                   
Example: `0xb0 0x82 0x02 0x80`
- ElementID: `0xb0` - this is a PixelWidth element
- Length: `0x82` = `10000010` - length of the data is 2 octets (bytes) (see [VINT](#VINT) for details)
- Data: `0x02 0x80` = `640` - the content of the element, in this case an uint (because PixelWidth is an UintNode element)


## Document structure of an mkv/webm file

MKV and webm (webm is a subset of mkv) have the following document structure:
- EBML (master) - this is the ebml header node, all ebml document must start with this
  - EBMLVersion (uint)
  - EBMLReadVersion (uint)
  - ...
  - DocType (string) - eg `webm`
- Segment (master) - this is the top-level document for mkv/webm files
  - SeekHead (master) - contains positions in the file for other 2nd level nodes
    - Seek (master)
      - SeekID (binary) - the ElementID of the referenced node (eg: `0x15, 0x49, 0xa9, 0x66` for Info node)
      - SeekPosition (uint) - the position of the referenced node in octets (bytes), relative to the start of the Segment data
    - Seek (master)
      - SeekID (binary)
    - ...
  - Info (master)
    - TimestampScale (uint) - defines the scale of all durations in the document, in nanoseconds,
                              eg: `1000000` means a duration of `1` is 1000000 nanoseconds = 1 millisecond
    - Duration (uint) - length of the video, unit is based on TimestampScale
    - ...
  - Tracks (master)
    - TrackEntry (master)
      - TrackType (uint) - 1 for video, 2 for audio
      - CodecID (string) - eg: `V_VP8` for vp8 video, `A_OPUS` for opus audio
      - ...
    - TrackEntry (master)
      - TrackType (uint)
      - ...
  - Cluster
    - Timestamp (uint) - timestamp relative to the first Cluster.Timestamp (0 for the first Cluster),
                         unit is based on Info.TimestampScale
    - SimpleBlock (binary) - the actual video/audio frame, although the node type is binary, the data is further structured,
                             see [SimpleBlock Structure](https://www.matroska.org/technical/specs/index.html#simpleblock_structure)
    - SimpleBlock (binary)
    - ...

## Element types

### Built in EBML elements

- [ebml/const.go](ebml/const.go)
- [ebml spec](https://github.com/cellar-wg/ebml-specification/blob/master/specification.markdown#ebml-document)

### Webm/mkv elements

- [webm/const_gen.go](webm/const_gen.go)
- [mkv spec](https://matroska.org/technical/specs/index.html)


## VINT
VINT is a simple encoding for unsigned integer of any length. The leading zeros define the length of the VINT
in octets (bytes) minus 0 (eg: 3 leading zeros mean a length of 4), followed by a `1`, followed by the bits
of the actual number. 

Example: `0x84` = `10000100`
- there are no leading `0`, meaning a length of 0+1=1 byte
- followed by a `1`, which is not part of the number
- followed by `0000100` which is 4
  
Example: `0x42 0x82` = `01000010 10000010`
- there is one leading `0`, meaning a length of 1+1=2 bytes
- followed by a `1`, which is not part of the number
- followed by `000010 10000010`, which is 512+128+2=642

# Usage

## Reading

```
f, err := os.Open('/tmp/example.webm')
if err != nil {
    return panic(err)
}

formatter := webm.NewFormatter()

root, err := formatter.Parse(f)
if err != nil {
    return panic(err)
}

muxingApp := root.FindFirst(webm.TypeSegment, webm.TypeInfo, webm.TypeMuxingApp)
fmt.Println(muxingApp.(*ebml.StringNode).Value())
```


## Writing

Writing an ebml document with only the EBM header:
```
out, err := os.Create('/tmp/example.webm')
if err != nil {
    panic(err)
}

formatter := webm.NewFormatter()

head := ebml.TypeEBML.New(f.Registry())
head.AddChild(ebml.TypeEBMLVersion.New().SetValue(1))
head.AddChild(ebml.TypeEBMLReadVersion.New().SetValue(1))
head.AddChild(ebml.TypeEBMLMaxIDLength.New().SetValue(4))
head.AddChild(ebml.TypeEBMLMaxSizeLength.New().SetValue(8))
head.AddChild(ebml.TypeDocType.New().SetValue("webm"))
head.AddChild(ebml.TypeDocTypeVersion.New().SetValue(1))
head.AddChild(ebml.TypeDocTypeReadVersion.New().SetValue(1))

formatter.WriteNodes(out, head)
```

## Advanced examples

For more advanced usage see:
- [examples/main.go](examples/main.go)

# Useful 3rd party tools

## MKVToolNix

MKVToolNix is a very useful tool for analyzing an mkv/webm file:
  - https://mkvtoolnix.download/
  - use the Info tool to display the structure of the mkv file

![mkvtoolnix](docs/mkvtoolnix.png)

## Example webm files for testing
  - https://www.webmfiles.org/demo-files/
  - https://tools.woolyss.com/html5-audio-video-tester/

