package ebml

import (
	"encoding/binary"
	"io"
	"log"

	"github.com/pkg/errors"
)

type NodeTypeInt struct {
	NodeTypeBase
}

type IntNode struct {
	typ      NodeType
	dataLen  VINT
	totalLen uint64
	value    int64
}

func NewNodeTypeInt(name string, id VINT) *NodeTypeInt {
	return &NodeTypeInt{NodeTypeBase: NewNodeTypeBase(name, id)}
}

func (t *NodeTypeInt) New() *IntNode {
	length := VINTFromUint64(0)
	n := &IntNode{
		typ:      t,
		dataLen:  length,
		totalLen: uint64(t.ID().Len()) + length.Value(),
	}
	n.calcTotalLen()
	return n
}

func (t *NodeTypeInt) NewNode(*Registry) Node {
	return t.New()
}

func (n *IntNode) Type() NodeType {
	return n.typ
}

func (n *IntNode) UnknownLength() bool {
	return false
}

func (n *IntNode) DataLen() uint64 {
	return n.dataLen.Value()
}

func (n *IntNode) TotalLen() uint64 {
	return n.totalLen
}

func (n *IntNode) Value() int64 {
	return n.value
}

func (n *IntNode) SetValue(v int64) *IntNode {
	n.value = v
	n.dataLen = VINTFromUint64(uint64(len(n.bytes())))
	n.calcTotalLen()

	return n
}

func (n *IntNode) WriteTo(writer io.Writer) error {
	err := WriteVINT(writer, n.dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write(n.bytes())
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (n *IntNode) ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error) {
	length, read, err := ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.dataLen = length
	n.calcTotalLen()

	log.Printf("reading int of type: %s (%s) length: %d", n.typ.Name(), n.typ.ID().Hex(), n.dataLen.Value())

	if length.Value() == 0 {
		n.value = 0
		return nil, totalRead, nil
	}

	buf := make([]byte, 8)
	read, err = reader.Read(buf[8-length.Value():])
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}

	n.value = bytesToInt64(buf, length.Value())

	return nil, totalRead, nil
}

func (n *IntNode) bytes() []byte {
	return int64ToBytes(n.value)
}

func bytesToInt64(buf []byte, size uint64) int64 {
	neg := (buf[8-size] & 128) != 0

	if neg {
		for i := uint64(0); i < 8-size; i++ {
			buf[i] = 255
		}
	}

	return int64(binary.BigEndian.Uint64(buf))
}

func int64ToBytes(v int64) []byte {
	if v == 0 {
		return nil
	}

	neg := v < 0
	data := make([]byte, 8)
	binary.BigEndian.PutUint64(data, uint64(v))
	var (
		i int
		b byte
	)
	if neg {
		for i, b = range data {
			if b != 255 {
				break
			}
		}
		if data[i]&128 != 0 {
			return data[i:]
		}

		return data[i-1:]
	}

	for i, b = range data {
		if b != 0 {
			break
		}
	}
	if data[i]&128 == 0 {
		return data[i:]
	}
	return data[i-1:]
}

func (n *IntNode) calcTotalLen() {
	n.totalLen = uint64(n.typ.ID().Len()) + uint64(n.dataLen.Len()) + n.dataLen.Value()
}
