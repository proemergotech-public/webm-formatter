package ebml

import (
	"io"
	"log"

	"github.com/pkg/errors"
)

type NodeTypeUTF8 struct {
	NodeTypeBase
}

type UTF8Node struct {
	typ      NodeType
	dataLen  VINT
	totalLen uint64
	value    string
}

func NewNodeTypeUTF8(name string, id VINT) *NodeTypeUTF8 {
	return &NodeTypeUTF8{NodeTypeBase: NewNodeTypeBase(name, id)}
}

func (t *NodeTypeUTF8) New() *UTF8Node {
	length := VINTFromUint64(0)
	n := &UTF8Node{
		typ:      t,
		dataLen:  length,
		totalLen: uint64(t.ID().Len()) + length.Value(),
	}
	n.calcTotalLen()
	return n
}

func (t *NodeTypeUTF8) NewNode(*Registry) Node {
	return t.New()
}

func (n *UTF8Node) Type() NodeType {
	return n.typ
}

func (n *UTF8Node) UnknownLength() bool {
	return false
}

func (n *UTF8Node) DataLen() uint64 {
	return n.dataLen.Value()
}

func (n *UTF8Node) TotalLen() uint64 {
	return n.totalLen
}

func (n *UTF8Node) Value() string {
	return n.value
}

func (n *UTF8Node) SetValue(v string) *UTF8Node {
	n.value = v
	n.dataLen = VINTFromUint64(uint64(len(n.value)))
	n.calcTotalLen()

	return n
}

func (n *UTF8Node) WriteTo(writer io.Writer) error {
	err := WriteVINT(writer, n.dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write([]byte(n.value))
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (n *UTF8Node) ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error) {
	length, read, err := ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.dataLen = length
	n.calcTotalLen()

	log.Printf("reading utf8 of type: %s (%s) length: %d", n.typ.Name(), n.typ.ID().Hex(), n.dataLen.Value())
	buf := make([]byte, length.Value())
	read, err = reader.Read(buf)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.value = string(buf)

	return nil, totalRead, nil
}

func (n *UTF8Node) calcTotalLen() {
	n.totalLen = uint64(n.typ.ID().Len()) + uint64(n.dataLen.Len()) + n.dataLen.Value()
}
