package ebml

import (
	"io"
	"log"
	"math"

	"github.com/pkg/errors"
)

type NodeTypeFloat struct {
	NodeTypeBase
}

type FloatNode struct {
	typ      NodeType
	dataLen  VINT
	totalLen uint64
	value64  float64
	value32  float32
	bitSize  int
}

func NewNodeTypeFloat(name string, id VINT) *NodeTypeFloat {
	return &NodeTypeFloat{NodeTypeBase: NewNodeTypeBase(name, id)}
}

func (t *NodeTypeFloat) New() *FloatNode {
	length := VINTFromUint64(0)
	n := &FloatNode{
		typ:      t,
		dataLen:  length,
		totalLen: uint64(t.ID().Len()) + length.Value(),
	}
	n.calcTotalLen()
	return n
}

func (t *NodeTypeFloat) NewNode(*Registry) Node {
	return t.New()
}

func (n *FloatNode) Type() NodeType {
	return n.typ
}

func (n *FloatNode) UnknownLength() bool {
	return false
}

func (n *FloatNode) DataLen() uint64 {
	return n.dataLen.Value()
}

func (n *FloatNode) TotalLen() uint64 {
	return n.totalLen
}

func (n *FloatNode) Value() float64 {
	return n.Value64()
}

func (n *FloatNode) Value32() float32 {
	switch n.bitSize {
	case 4:
		return n.value32
	case 8:
		return float32(n.value64)
	}

	return 0
}

func (n *FloatNode) Value64() float64 {
	switch n.bitSize {
	case 4:
		return float64(n.value32)
	case 8:
		return n.value64
	}

	return 0
}

func (n *FloatNode) SetValue(v float64) *FloatNode {
	n.SetValue64(v)
	return n
}

func (n *FloatNode) SetValue32(v float32) *FloatNode {
	if v == 0 {
		n.value64 = 0
		n.value32 = 0
		n.bitSize = 0
		n.dataLen = VINTFromUint64(0)
		n.calcTotalLen()
		return n
	}

	n.value32 = v
	n.value64 = 0
	n.bitSize = 4
	n.dataLen = VINTFromUint64(4)
	n.calcTotalLen()

	return n
}

func (n *FloatNode) SetValue64(v float64) *FloatNode {
	if v == 0 {
		n.value64 = 0
		n.value32 = 0
		n.bitSize = 0
		n.dataLen = VINTFromUint64(0)
		n.calcTotalLen()
		return n
	}

	n.value64 = v
	n.value32 = 0
	n.bitSize = 8
	n.dataLen = VINTFromUint64(8)
	n.calcTotalLen()

	return n
}

func (n *FloatNode) WriteTo(writer io.Writer) error {
	err := WriteVINT(writer, n.dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write(n.bytes())
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (n *FloatNode) ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error) {
	length, read, err := ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.dataLen = length
	n.calcTotalLen()

	log.Printf("reading float of type: %s (%s) length: %d", n.typ.Name(), n.typ.ID().Hex(), n.dataLen.Value())

	switch length.Value() {
	case 0:
		n.value32 = 0
		n.value64 = 0
		n.bitSize = 0
	case 4:
		buf := make([]byte, 4)
		read, err = reader.Read(buf)
		totalRead += uint64(read)
		if err != nil {
			return nil, totalRead, err
		}

		n.value32 = math.Float32frombits(
			(uint32(buf[0]) << 24) +
				(uint32(buf[1]) << 16) +
				(uint32(buf[2]) << 8) +
				uint32(buf[3]),
		)
		n.value64 = 0
		n.bitSize = 4
	case 8:
		buf := make([]byte, 8)
		read, err = reader.Read(buf)
		totalRead += uint64(read)
		if err != nil {
			return nil, totalRead, err
		}

		n.value64 = math.Float64frombits(
			(uint64(buf[0]) << 56) +
				(uint64(buf[1]) << 48) +
				(uint64(buf[2]) << 40) +
				(uint64(buf[3]) << 32) +
				(uint64(buf[4]) << 24) +
				(uint64(buf[5]) << 16) +
				(uint64(buf[6]) << 8) +
				uint64(buf[7]),
		)
		n.value32 = 0
		n.bitSize = 8

	default:
		return nil, totalRead, errors.Errorf("unsupported length for float: %d", length.Value())
	}

	return nil, totalRead, nil
}

func (n *FloatNode) bytes() []byte {
	switch n.bitSize {
	case 4:
		buf := make([]byte, 4)
		uval := math.Float32bits(n.value32)
		buf[0] = byte(uval >> 24)
		buf[1] = byte(uval >> 16)
		buf[2] = byte(uval >> 8)
		buf[3] = byte(uval)
		return buf

	case 8:
		buf := make([]byte, 8)
		uval := math.Float64bits(n.value64)
		buf[0] = byte(uval >> 56)
		buf[1] = byte(uval >> 48)
		buf[2] = byte(uval >> 40)
		buf[3] = byte(uval >> 32)
		buf[4] = byte(uval >> 24)
		buf[5] = byte(uval >> 16)
		buf[6] = byte(uval >> 8)
		buf[7] = byte(uval)

		return buf
	}

	return nil
}

func (n *FloatNode) calcTotalLen() {
	n.totalLen = uint64(n.typ.ID().Len()) + uint64(n.dataLen.Len()) + n.dataLen.Value()
}
