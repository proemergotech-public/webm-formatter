package ebml

type Registry struct {
	nodeTypes map[string]NodeType
}

func NewRegistry() *Registry {
	r := &Registry{
		nodeTypes: make(map[string]NodeType),
	}
	r.AddType(TypeVoid)

	r.AddType(TypeEBML)
	r.AddType(TypeEBMLVersion)
	r.AddType(TypeEBMLReadVersion)
	r.AddType(TypeEBMLMaxIDLength)
	r.AddType(TypeEBMLMaxSizeLength)
	r.AddType(TypeDocType)
	r.AddType(TypeDocTypeVersion)
	r.AddType(TypeDocTypeReadVersion)
	r.AddType(TypeDocTypeExtension)
	r.AddType(TypeDocTypeExtensionName)
	r.AddType(TypeDocTypeExtensionVersion)

	return r
}

func (r *Registry) AddType(typ NodeType) {
	r.nodeTypes[typ.Key()] = typ
}
