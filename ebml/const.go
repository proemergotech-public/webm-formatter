package ebml

var (
	TypeEBML                    = NewNodeTypeMaster("EBML", MustVINTFromBytes([]byte{0x1a, 0x45, 0xdf, 0xa3}), TypeEBMLVersion, TypeEBMLReadVersion, TypeEBMLMaxIDLength, TypeEBMLMaxSizeLength, TypeDocType, TypeDocTypeVersion, TypeDocTypeReadVersion, TypeDocTypeExtension)
	TypeEBMLVersion             = NewNodeTypeUint("EBMLVersion", MustVINTFromBytes([]byte{0x42, 0x86}))
	TypeEBMLReadVersion         = NewNodeTypeUint("EBMLReadVersion", MustVINTFromBytes([]byte{0x42, 0xf7}))
	TypeEBMLMaxIDLength         = NewNodeTypeUint("EBMLMaxIDLength", MustVINTFromBytes([]byte{0x42, 0xf2}))
	TypeEBMLMaxSizeLength       = NewNodeTypeUint("EBMLMaxSizeLength", MustVINTFromBytes([]byte{0x42, 0xf3}))
	TypeDocType                 = NewNodeTypeString("DocType", MustVINTFromBytes([]byte{0x42, 0x82}))
	TypeDocTypeVersion          = NewNodeTypeUint("DocTypeVersion", MustVINTFromBytes([]byte{0x42, 0x87}))
	TypeDocTypeReadVersion      = NewNodeTypeUint("DocTypeReadVersion", MustVINTFromBytes([]byte{0x42, 0x85}))
	TypeDocTypeExtension        = NewNodeTypeMaster("DocTypeExtension", MustVINTFromBytes([]byte{0x42, 0x81}), TypeDocTypeExtensionName, TypeDocTypeExtensionVersion)
	TypeDocTypeExtensionName    = NewNodeTypeString("DocTypeExtensionName", MustVINTFromBytes([]byte{0x42, 0x83}))
	TypeDocTypeExtensionVersion = NewNodeTypeUint("DocTypeExtensionVersion", MustVINTFromBytes([]byte{0x42, 0x84}))

	TypeVoid = NewNodeTypeBinary("Void", MustVINTFromBytes([]byte{0xec}))
)
