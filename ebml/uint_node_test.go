package ebml

import (
	"bytes"
	"fmt"
	"testing"
)

func TestUintNode_ReadFrom(t *testing.T) {
	for _, test := range []struct {
		bytes []byte
		want  uint64
	}{
		{
			bytes: []byte{},
			want:  0,
		},
		{
			bytes: []byte{0x00},
			want:  0,
		},
		{
			bytes: []byte{0x01},
			want:  1,
		},
		{
			bytes: []byte{0x7f},
			want:  127,
		},
		{
			bytes: []byte{0x80},
			want:  128,
		},
		{
			bytes: []byte{0xff},
			want:  255,
		},
		{
			bytes: []byte{0x01, 0x00},
			want:  256,
		},
		{
			bytes: []byte{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
			want:  18446744073709551615,
		},
	} {
		test := test
		t.Run(fmt.Sprintf("%d", test.want), func(t *testing.T) {
			length := VINTFromUint64(uint64(len(test.bytes)))

			node := NewNodeTypeUint("test", VINT{}).New()
			_, _, err := node.ReadFrom(bytes.NewReader(append(length.Bytes(), test.bytes...)))
			if err != nil {
				t.Fatalf("%+v", err)
			}

			if test.want != node.value {
				t.Errorf("want/got: %d, %d", test.want, node.value)
			}
		})
	}
}

func TestUintNode_Bytes(t *testing.T) {
	for _, test := range []struct {
		value uint64
		want  []byte
	}{
		{
			value: 0,
			want:  []byte{},
		},
		{
			value: 1,
			want:  []byte{0x01},
		},
		{
			value: 127,
			want:  []byte{0x7f},
		},
		{
			value: 128,
			want:  []byte{0x80},
		},
		{
			value: 255,
			want:  []byte{0xff},
		},
		{
			value: 256,
			want:  []byte{0x01, 0x00},
		},
		{
			value: 18446744073709551615,
			want:  []byte{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
		},
	} {
		test := test
		t.Run(fmt.Sprintf("%d", test.value), func(t *testing.T) {
			node := &UintNode{
				typ:   NewNodeTypeUint("test", VINT{}),
				value: test.value,
			}
			got := node.bytes()

			if !bytes.Equal(test.want, got) {
				t.Errorf("want/got: %d, %d", test.want, got)
			}
		})
	}
}
