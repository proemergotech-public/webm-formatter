package ebml

import (
	"encoding/binary"
	"io"
	"log"

	"github.com/pkg/errors"
)

type NodeTypeUint struct {
	NodeTypeBase
}

type UintNode struct {
	typ      NodeType
	dataLen  VINT
	totalLen uint64
	value    uint64
}

func NewNodeTypeUint(name string, id VINT) *NodeTypeUint {
	return &NodeTypeUint{NodeTypeBase: NewNodeTypeBase(name, id)}
}

func (t *NodeTypeUint) New() *UintNode {
	length := VINTFromUint64(0)
	n := &UintNode{
		typ:     t,
		dataLen: length,
	}
	n.calcTotalLen()
	return n
}

func (t *NodeTypeUint) NewNode(*Registry) Node {
	return t.New()
}

func (n *UintNode) Type() NodeType {
	return n.typ
}

func (n *UintNode) UnknownLength() bool {
	return false
}

func (n *UintNode) DataLen() uint64 {
	return n.dataLen.Value()
}

func (n *UintNode) TotalLen() uint64 {
	return n.totalLen
}

func (n *UintNode) Value() uint64 {
	return n.value
}

func (n *UintNode) SetValue(v uint64) *UintNode {
	n.value = v
	n.dataLen = VINTFromUint64(uint64(len(n.bytes())))
	n.calcTotalLen()

	return n
}

func (n *UintNode) WriteTo(writer io.Writer) error {
	err := WriteVINT(writer, n.dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write(n.bytes())
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (n *UintNode) ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error) {
	length, read, err := ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.dataLen = length
	n.calcTotalLen()

	log.Printf("reading uint of type: %s (%s) length: %d", n.typ.Name(), n.typ.ID().Hex(), n.dataLen.Value())

	size := length.Value()
	if size == 0 {
		n.value = 0
		return nil, totalRead, nil
	}

	buf := make([]byte, 8)
	read, err = reader.Read(buf[8-size:])
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.value = binary.BigEndian.Uint64(buf)

	return nil, totalRead, nil
}

func (n *UintNode) bytes() []byte {
	if n.value == 0 {
		return nil
	}

	data := make([]byte, 8)
	binary.BigEndian.PutUint64(data, n.value)
	var (
		i int
		b byte
	)
	for i, b = range data {
		if b != 0 {
			break
		}
	}
	return data[i:]
}

func (n *UintNode) calcTotalLen() {
	n.totalLen = uint64(n.typ.ID().Len()) + uint64(n.dataLen.Len()) + n.dataLen.Value()
}
