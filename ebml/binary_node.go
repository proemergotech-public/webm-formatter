package ebml

import (
	"io"
	"log"

	"github.com/pkg/errors"
)

type NodeTypeBinary struct {
	NodeTypeBase
}

type BinaryNode struct {
	typ      NodeType
	dataLen  VINT
	totalLen uint64
	value    []byte
}

func NewNodeTypeBinary(name string, id VINT) *NodeTypeBinary {
	return &NodeTypeBinary{NodeTypeBase: NewNodeTypeBase(name, id)}
}

func (t *NodeTypeBinary) New() *BinaryNode {
	length := VINTFromUint64(0)
	n := &BinaryNode{
		typ:      t,
		dataLen:  length,
		totalLen: uint64(t.ID().Len()) + length.Value(),
	}
	n.calcTotalLen()
	return n
}

func (t *NodeTypeBinary) NewNode(*Registry) Node {
	return t.New()
}

func (n *BinaryNode) Type() NodeType {
	return n.typ
}

func (n *BinaryNode) UnknownLength() bool {
	return false
}

func (n *BinaryNode) DataLen() uint64 {
	return n.dataLen.Value()
}

func (n *BinaryNode) TotalLen() uint64 {
	return n.totalLen
}

func (n *BinaryNode) Value() []byte {
	return n.value
}

func (n *BinaryNode) SetValue(v []byte) *BinaryNode {
	n.value = v
	n.dataLen = VINTFromUint64(uint64(len(n.value)))
	n.calcTotalLen()

	return n
}

func (n *BinaryNode) WriteTo(writer io.Writer) error {
	err := WriteVINT(writer, n.dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write(n.value)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (n *BinaryNode) ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error) {
	length, read, err := ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.dataLen = length
	n.calcTotalLen()

	log.Printf("reading binary of type: %s (%s) length: %d", n.typ.Name(), n.typ.ID().Hex(), n.dataLen.Value())
	buf := make([]byte, n.dataLen.Value())
	read, err = reader.Read(buf)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.value = buf

	return nil, totalRead, nil
}

func (n *BinaryNode) calcTotalLen() {
	n.totalLen = uint64(n.typ.ID().Len()) + uint64(n.dataLen.Len()) + n.dataLen.Value()
}
