package ebml

import (
	"io"
)

type Node interface {
	Type() NodeType
	UnknownLength() bool
	DataLen() uint64
	TotalLen() uint64
	WriteTo(writer io.Writer) error

	ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error)
}
