package ebml

import (
	"io"

	"github.com/pkg/errors"
)

type Formatter struct {
	registry        *Registry
	allowedTopLevel []NodeType
}

func NewFormatter(registry *Registry) *Formatter {
	return &Formatter{
		registry:        registry,
		allowedTopLevel: []NodeType{TypeEBML},
	}
}

func (f *Formatter) Registry() *Registry {
	return f.registry
}

func (f *Formatter) AddTopLevelType(typ NodeType) {
	f.allowedTopLevel = append(f.allowedTopLevel, typ)
}

func (f *Formatter) Parse(reader io.Reader) (*MasterNode, error) {
	root := NewNodeTypeMaster("root", VINT{}, f.allowedTopLevel...).New(f.registry)
	root.root = true
	_, _, err := root.ReadFrom(reader)
	if err != nil && err != io.EOF {
		return nil, err
	}

	return root, nil
}

func (f *Formatter) WriteNodes(writer io.Writer, nodes ...Node) error {
	for _, n := range nodes {
		err := WriteVINT(writer, n.Type().ID())
		if err != nil {
			return errors.WithStack(err)
		}

		err = n.WriteTo(writer)
		if err != nil {
			return err
		}
	}

	return nil
}
