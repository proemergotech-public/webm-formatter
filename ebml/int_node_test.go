package ebml

import (
	"bytes"
	"fmt"
	"testing"
)

func TestIntNode_ReadFrom(t *testing.T) {
	for _, test := range []struct {
		bytes []byte
		want  int64
	}{
		{
			bytes: []byte{0x00},
			want:  0,
		},
		{
			bytes: []byte{},
			want:  0,
		},
		{
			bytes: []byte{0x01},
			want:  1,
		},
		{
			bytes: []byte{0xff},
			want:  -1,
		},
		{
			bytes: []byte{0x7f},
			want:  127,
		},
		{
			bytes: []byte{0x81},
			want:  -127,
		},
		{
			bytes: []byte{0x00, 0x80},
			want:  128,
		},
		{
			bytes: []byte{0x80},
			want:  -128,
		},
		{
			bytes: []byte{0x00, 0xff},
			want:  255,
		},
		{
			bytes: []byte{0xff, 0x01},
			want:  -255,
		},
		{
			bytes: []byte{0x01, 0x00},
			want:  256,
		},
		{
			bytes: []byte{0xff, 0x00},
			want:  -256,
		},
		{
			bytes: []byte{0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
			want:  9223372036854775807,
		},
		{
			bytes: []byte{0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
			want:  -9223372036854775808,
		},
	} {
		test := test
		t.Run(fmt.Sprintf("%d", test.want), func(t *testing.T) {
			length := VINTFromUint64(uint64(len(test.bytes)))

			node := NewNodeTypeInt("test", VINT{}).New()
			_, _, err := node.ReadFrom(bytes.NewReader(append(length.Bytes(), test.bytes...)))
			if err != nil {
				t.Fatalf("%+v", err)
			}

			if test.want != node.value {
				t.Errorf("want/got: %d, %d", test.want, node.value)
			}
		})
	}
}

func TestIntNode_Bytes(t *testing.T) {
	for _, test := range []struct {
		value int64
		want  []byte
	}{
		{
			value: 0,
			want:  []byte{},
		},
		{
			value: 1,
			want:  []byte{0x01},
		},
		{
			value: -1,
			want:  []byte{0xff},
		},
		{
			value: 127,
			want:  []byte{0x7f},
		},
		{
			value: -127,
			want:  []byte{0x81},
		},
		{
			value: 128,
			want:  []byte{0x00, 0x80},
		},
		{
			value: -128,
			want:  []byte{0x80},
		},
		{
			value: 255,
			want:  []byte{0x00, 0xff},
		},
		{
			value: -255,
			want:  []byte{0xff, 0x01},
		},
		{
			value: 256,
			want:  []byte{0x01, 0x00},
		},
		{
			value: -256,
			want:  []byte{0xff, 0x00},
		},
		{
			value: 9223372036854775807,
			want:  []byte{0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
		},
		{
			value: -9223372036854775808,
			want:  []byte{0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
		},
	} {
		test := test
		t.Run(fmt.Sprintf("%d", test.value), func(t *testing.T) {
			node := &IntNode{
				typ:   NewNodeTypeInt("test", VINT{}),
				value: test.value,
			}
			got := node.bytes()

			if !bytes.Equal(test.want, got) {
				t.Errorf("want/got: %d, %d", test.want, got)
			}
		})
	}
}
