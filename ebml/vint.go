package ebml

import (
	"encoding/binary"
	"encoding/hex"
	"io"

	"github.com/pkg/errors"
)

type VINT struct {
	formatted []byte
	value     uint64
}

func MustVINTFromBytes(data []byte) VINT {
	vint, err := VINTFromBytes(data)
	if err != nil {
		panic(err)
	}

	return vint
}

func VINTFromBytes(data []byte) (VINT, error) {
	length := len(data)

	markerByte := (length - 1) / 8
	marker := byte(1) << (7 - byte((length-1)%8))
	for i := 0; i < (length-1)/8; i++ {
		if data[i] != 0 {
			return VINT{}, errors.Errorf("invalid vint: %v", data)
		}
	}

	markedByte := data[markerByte]
	if markedByte&marker == 0 {
		return VINT{}, errors.Errorf("invalid vint: %v", data)
	}

	if markedByte&(marker-1) != markedByte^marker {
		return VINT{}, errors.Errorf("invalid vint: %v", data)
	}

	return vintFromBytes(data), nil
}

func vintFromBytes(data []byte) VINT {

	length := len(data)
	marker := byte(1) << (7 - byte((length-1)%8))
	if length <= 8 {
		bytes8 := make([]byte, 8)
		copy(bytes8[8-length:], data)
		bytes8[8-length] ^= marker

		return VINT{
			formatted: data,
			value:     binary.BigEndian.Uint64(bytes8),
		}
	}

	vint := VINT{
		formatted: data,
		value:     binary.BigEndian.Uint64(data[length-8:]),
	}
	if length == 9 {
		vint.value -= uint64(1) << 63
	}

	return vint
}

func (v VINT) Bytes() []byte {
	return v.formatted
}

func (v VINT) Value() uint64 {
	return v.value
}

func (v VINT) UnknownLength() bool {
	return unknownLength(v.value, uint64(len(v.formatted)))
}

func (v VINT) Len() int {
	return len(v.formatted)
}

func unknownLength(value uint64, length uint64) bool {
	if length <= 8 {
		return value == (uint64(1)<<(7*length))-1
	} else if length == 9 {
		return value == 9223372036854775807
	}

	return value == 18446744073709551615
}

func VINTFromUint64(value uint64) VINT {
	for i := uint64(0); i < 8; i++ {
		if value < (uint64(1)<<(7*(i+1))) && !unknownLength(value, i+1) {
			data := make([]byte, 8)
			binary.BigEndian.PutUint64(data, value)
			data[7-i] |= 1 << (7 - i)
			return VINT{
				formatted: data[7-i:],
				value:     value,
			}
		}
	}

	data := make([]byte, 9)
	binary.BigEndian.PutUint64(data[1:], value)
	if value == 18446744073709551615 {
		data[0] |= 64
	} else {
		data[0] |= 128
	}

	return VINT{
		formatted: data,
		value:     value,
	}
}

func ReadVINT(r io.Reader) (VINT, int, error) {
	var (
		length    byte = 1
		i         byte
		b         = make([]byte, 1)
		err       error
		read      int
		totalRead int
	)
WIDTH:
	for {
		read, err = r.Read(b)
		totalRead += read
		if err != nil {
			return VINT{}, totalRead, err
		}

		for i = 7; ; i-- {
			if b[0]&(1<<i) != 0 {
				break WIDTH
			}
			length++

			if i == 0 {
				break
			}
		}
	}

	data := make([]byte, length)
	read, err = r.Read(data[totalRead:])
	data[totalRead-1] = b[0]
	totalRead += read
	if err != nil {
		return VINT{}, totalRead, err
	}

	return vintFromBytes(data), totalRead, nil
}

func WriteVINT(w io.Writer, vint VINT) error {
	_, err := w.Write(vint.formatted)
	return err
}

func UnknownLengthVINT() VINT {
	return VINT{
		formatted: []byte{255},
		value:     127,
	}
}

func (v VINT) Hex() string {
	res := make([]byte, 2*len(v.formatted))
	hex.Encode(res, v.formatted)

	return string(res)
}

func (v VINT) Equals(v2 VINT) bool {
	if v.Len() != v2.Len() {
		return false
	}

	for i := range v.formatted {
		if v.formatted[i] != v2.formatted[i] {
			return false
		}
	}

	return true
}
