package ebml

import (
	"io"
	"log"
	"sync"

	"github.com/pkg/errors"
)

type NodeTypeMaster struct {
	NodeTypeBase
	allowedChildren map[string]bool
}

type MasterNode struct {
	registry               *Registry
	typ                    NodeType
	children               []Node
	allowedChildren        map[string]bool
	streaming              bool
	root                   bool
	dropChildrenAfterWrite bool

	childrenChan  chan Node
	childrenChanM sync.Mutex
	m             sync.Mutex
}

func NewNodeTypeMaster(name string, id VINT, allowedChildren ...NodeType) *NodeTypeMaster {
	allowedChildrenM := make(map[string]bool, len(allowedChildren)+1)
	for _, c := range allowedChildren {
		allowedChildrenM[c.Key()] = true
	}
	allowedChildrenM[TypeVoid.Key()] = true

	return &NodeTypeMaster{NodeTypeBase: NewNodeTypeBase(name, id), allowedChildren: allowedChildrenM}
}

func (t *NodeTypeMaster) New(r *Registry) *MasterNode {
	return &MasterNode{
		registry:        r,
		typ:             t,
		allowedChildren: t.allowedChildren,
	}
}

func (t *NodeTypeMaster) NewNode(r *Registry) Node {
	return t.New(r)
}

func (n *MasterNode) Type() NodeType {
	return n.typ
}

func (n *MasterNode) UnknownLength() bool {
	n.m.Lock()
	defer n.m.Unlock()
	if n.streaming {
		return true
	}

	for _, c := range n.children {
		if c.UnknownLength() {
			return true
		}
	}

	return false
}

func (n *MasterNode) DataLen() uint64 {
	n.m.Lock()
	defer n.m.Unlock()
	var length uint64
	for _, c := range n.children {
		length += c.TotalLen()
	}
	return length
}

func (n *MasterNode) TotalLen() uint64 {
	dataLen := VINTFromUint64(n.DataLen())
	return uint64(n.typ.ID().Len()) + uint64(dataLen.Len()) + dataLen.Value()
}

func (n *MasterNode) Children() []Node {
	n.m.Lock()
	defer n.m.Unlock()
	return n.children
}

func (n *MasterNode) AddChild(c Node) *MasterNode {
	n.m.Lock()
	n.childrenChanM.Lock()

	streaming := n.streaming && n.childrenChan != nil
	if !n.dropChildrenAfterWrite || !streaming {
		n.children = append(n.children, c)
	}
	n.m.Unlock()

	if streaming {
		n.childrenChan <- c
	}
	n.childrenChanM.Unlock()

	return n
}

func (n *MasterNode) RemoveChildren() {
	n.m.Lock()
	defer n.m.Unlock()
	n.children = nil
}

func (n *MasterNode) Streaming() bool {
	n.m.Lock()
	defer n.m.Unlock()
	return n.streaming
}

func (n *MasterNode) SetStreaming(streaming bool) {
	n.m.Lock()
	n.childrenChanM.Lock()
	if n.streaming && !streaming && n.childrenChan != nil {
		close(n.childrenChan)
		n.childrenChan = nil
	}

	n.streaming = streaming
	
	n.m.Unlock()
	n.childrenChanM.Unlock()
}

func (n *MasterNode) DropChildrenAfterWrite() bool {
	n.m.Lock()
	defer n.m.Unlock()
	return n.dropChildrenAfterWrite
}

func (n *MasterNode) SetDropChildrenAfterWrite(dropChildrenAfterWrite bool) {
	n.m.Lock()
	defer n.m.Unlock()
	n.dropChildrenAfterWrite = dropChildrenAfterWrite
}

func (n *MasterNode) ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error) {
	var (
		length     = UnknownLengthVINT()
		exitLength uint64
		read       int
		childRead  uint64
		childID    *VINT
		childIDVal VINT
	)
	if !n.root {
		length, read, err = ReadVINT(reader)
		totalRead += uint64(read)
		if err != nil {
			return nil, totalRead, err
		}
		if !length.UnknownLength() {
			exitLength = length.Value() + uint64(length.Len())
		}
	}

	if length.UnknownLength() {
		log.Printf("%s (%s): start, length: unknown", n.typ.Name(), n.typ.ID().Hex())
	} else {
		log.Printf("%s (%s): start, length: %d", n.typ.Name(), n.typ.ID().Hex(), length.Value())
	}
	defer func() {
		if err == nil {
			if length.UnknownLength() {
				log.Printf("%s (%s): end, totalRead: %d, length: unknown", n.typ.Name(), n.typ.ID().Hex(), totalRead)
			} else {
				log.Printf("%s (%s): end, totalRead: %d, length: %d", n.typ.Name(), n.typ.ID().Hex(), totalRead, length.Value())
			}
		}
	}()

	for {
		if childID == nil {
			childIDVal, read, err = ReadVINT(reader)
			totalRead += uint64(read)
			if err != nil {
				return nil, totalRead, err
			}
			childID = &childIDVal
		}

		childTyp, ok := n.registry.nodeTypes[string(childID.Bytes())]
		if !ok {
			return childID, totalRead, errors.Errorf("unknown type: %s found in node: %s (%s)", childID.Hex(), n.typ.Name(), n.typ.ID().Hex())
		}

		if !n.allowedChildren[string(childID.Bytes())] {
			if length.UnknownLength() {
				return childID, totalRead, nil
			}

			return childID, totalRead, errors.Errorf("master node of type: %s (%s) can not handle node of type: %s (%s)", n.typ.Name(), n.typ.ID().Hex(), childTyp.Name(), childID.Hex())
		}

		child := childTyp.NewNode(n.registry)
		n.children = append(n.children, child)

		orphanID, childRead, err = child.ReadFrom(reader)
		totalRead += childRead
		if err != nil {
			return orphanID, totalRead, err
		}

		childID = orphanID

		if !length.UnknownLength() && uint64(totalRead) >= exitLength {
			return childID, totalRead, nil
		}
	}
}

func (n *MasterNode) FindFirst(path ...NodeType) Node {
	if len(path) == 0 {
		return nil
	}

	id := path[0].ID()

	for _, c := range n.children {
		if !c.Type().ID().Equals(id) {
			continue
		}

		if len(path) == 1 {
			return c
		}

		if m, ok := c.(*MasterNode); ok {
			result := m.FindFirst(path[1:]...)
			if result != nil {
				return result
			}
		}
	}

	return nil
}

func (n *MasterNode) WriteTo(writer io.Writer) error {
	n.m.Lock()
	n.childrenChanM.Lock()
	chanLen := len(n.children)
	if chanLen < 1 {
		chanLen = 1
	}
	childrenChan := make(chan Node, chanLen)
	for _, c := range n.children {
		childrenChan <- c
	}
	if n.dropChildrenAfterWrite {
		n.children = nil
	}
	if n.streaming {
		n.childrenChan = childrenChan
	} else {
		close(childrenChan)
	}
	n.m.Unlock()
	n.childrenChanM.Unlock()

	var dataLen VINT
	if n.UnknownLength() {
		dataLen = UnknownLengthVINT()
	} else {
		dataLen = VINTFromUint64(n.DataLen())
	}
	err := WriteVINT(writer, dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	for c := range childrenChan {
		err := WriteVINT(writer, c.Type().ID())
		if err != nil {
			return errors.WithStack(err)
		}

		err = c.WriteTo(writer)
		if err != nil {
			return err
		}
	}

	return nil
}
