package ebml

import (
	"bytes"
	"strconv"
	"strings"
	"testing"
)

func TestVint_Read(t *testing.T) {
	for _, test := range []struct {
		raw               string
		want              uint64
		wantUnknownLength bool
	}{
		{
			raw:  "10000000",
			want: 0,
		},
		{
			raw:  "10000001",
			want: 1,
		},
		{
			raw:               "11111111",
			want:              127,
			wantUnknownLength: true,
		},
		{
			raw:  "01000000-01111111",
			want: 127,
		},
		{
			raw:  "01000000-10000000",
			want: 128,
		},
		{
			raw:               "01111111-11111111",
			want:              16383,
			wantUnknownLength: true,
		},
		{
			raw:               "00100000-00111111-11111111",
			want:              16383,
			wantUnknownLength: false,
		},
		{
			raw:               "00010000-00000000-00111111-11111111",
			want:              16383,
			wantUnknownLength: false,
		},
		{
			raw:               "00000000-11111111-11111111-11111111-11111111-11111111-11111111-11111111-11111111",
			want:              9223372036854775807,
			wantUnknownLength: true,
		},
		{
			raw:               "00000000-01000000-01111111-11111111-11111111-11111111-11111111-11111111-11111111-11111111",
			want:              9223372036854775807,
			wantUnknownLength: false,
		},
		{
			raw:               "00000000-01000000-11111111-11111111-11111111-11111111-11111111-11111111-11111111-11111111",
			want:              18446744073709551615,
			wantUnknownLength: true,
		},
		{
			raw:               "00000000-01011000-00000000-00000000-00000000-00000000-00000000-00000000-00000000-00000001",
			want:              1,
			wantUnknownLength: false,
		},
	} {
		test := test
		t.Run(test.raw, func(t *testing.T) {
			rawB := binaryStrToBytes(t, test.raw)

			vint, _, err := ReadVINT(bytes.NewReader(rawB))
			if err != nil {
				t.Fatalf("%+v", err)
			}

			got := vint.Value()
			if test.want != got {
				t.Errorf("want/got: %d/%d", test.want, got)
			}

			gotUnknownLength := vint.UnknownLength()
			if test.wantUnknownLength != gotUnknownLength {
				t.Errorf("want/got: %t/%t", test.wantUnknownLength, gotUnknownLength)
			}
		})

	}
}

func TestVINTFromUint64(t *testing.T) {
	for _, test := range []struct {
		want  string
		value uint64
	}{
		{
			value: 0,
			want:  "10000000",
		},
		{
			value: 1,
			want:  "10000001",
		},
		{
			value: 126,
			want:  "11111110",
		},
		{
			value: 127,
			want:  "01000000-01111111",
		},
		{
			value: 128,
			want:  "01000000-10000000",
		},
	} {
		vint := VINTFromUint64(test.value)
		got := bytesToBinaryStr(t, vint.Bytes())
		if got != test.want {
			t.Errorf("want/got: %s/%s", test.want, got)
		}

		if vint.UnknownLength() {
			t.Errorf("got unknown length")
		}
	}

}

//func TestVint_Write(t *testing.T) {
//	for _, test := range []struct {
//		value  string
//		allOne bool
//		want   string
//	}{
//		{
//			value:  "01111111",
//			allOne: true,
//			want:   "11111111",
//		},
//		{
//			value: "01111111",
//			want:  "01000000-01111111",
//		},
//		{
//			value: "11111111",
//			want:  "01000000-11111111",
//		},
//		{
//			value: "00111111-11111111",
//			want:  "01111111-11111111",
//		}, {
//			value: "11111111-11111111-11111111-11111111-11111111-11111111-11111111",
//			want:  "00000001-11111111-11111111-11111111-11111111-11111111-11111111-11111111",
//		}, {
//			value: "00000001-11111111-11111111-11111111-11111111-11111111-11111111",
//			want:  "00000011-11111111-11111111-11111111-11111111-11111111-11111111",
//		}, {
//			value: "11111111-11111111-11111111-11111111-11111111-11111111",
//			want:  "00000010-11111111-11111111-11111111-11111111-11111111-11111111",
//		}, {
//			value: "01111111-11111111-11111111-11111111-11111111-11111111-11111111-11111111",
//			want:  "00000000-11111111-11111111-11111111-11111111-11111111-11111111-11111111-11111111",
//		},
//	} {
//		test := test
//		t.Run(fmt.Sprintf("%s-%t", test.value, test.allOne), func(t *testing.T) {
//			valueB := binaryStrToBytes(t, test.value)
//			v := NewVInt()
//			v.allOne = test.allOne
//			v.SetBytes(valueB)
//
//			buff := bytes.NewBuffer(nil)
//			err := v.WriteTo(buff)
//			if err != nil {
//				t.Fatalf("%+v", err)
//			}
//
//			gotB := buff.Bytes()
//			got := bytesToBinaryStr(t, gotB)
//			if got != test.want {
//				t.Errorf("want/got: %s/%s", test.want, got)
//			}
//		})
//
//	}
//}
func binaryStrToBytes(t *testing.T, str string) []byte {
	if str == "" {
		return nil
	}

	t.Helper()
	parts := strings.Split(str, "-")
	data := make([]byte, 0, len(parts))
	for _, p := range parts {
		i, err := strconv.ParseUint(p, 2, 8)
		if err != nil {
			t.Fatalf("%+v", err)
		}
		data = append(data, byte(i))
	}

	return data
}

func bytesToBinaryStr(t *testing.T, data []byte) string {
	if len(data) == 0 {
		return ""
	}

	t.Helper()
	parts := make([]string, 0, len(data))
	for _, b := range data {
		p := strconv.FormatInt(int64(b), 2)
		parts = append(parts, strings.Repeat("0", 8-len(p))+p)
	}

	return strings.Join(parts, "-")
}
