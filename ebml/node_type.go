package ebml

type NodeType interface {
	Name() string
	ID() VINT
	Key() string
	NewNode(*Registry) Node
}

type NodeTypeBase struct {
	name string
	id   VINT
}

func NewNodeTypeBase(name string, id VINT) NodeTypeBase {
	return NodeTypeBase{
		name: name,
		id:   id,
	}
}

func (t *NodeTypeBase) Name() string {
	return t.name
}

func (t *NodeTypeBase) ID() VINT {
	return t.id
}

func (t *NodeTypeBase) Key() string {
	return string(t.id.Bytes())
}
