package ebml

import (
	"io"
	"log"
	"time"

	"github.com/pkg/errors"
)

var zeroTime = time.Date(2001, 01, 01, 0, 0, 0, 0, time.UTC)

type NodeTypeDate struct {
	NodeTypeBase
}

type DateNode struct {
	typ      NodeType
	dataLen  VINT
	totalLen uint64
	value    int64
}

func NewNodeTypeDate(name string, id VINT) *NodeTypeDate {
	return &NodeTypeDate{NodeTypeBase: NewNodeTypeBase(name, id)}
}

func (t *NodeTypeDate) New() *DateNode {
	length := VINTFromUint64(0)
	n := &DateNode{
		typ:      t,
		dataLen:  length,
		totalLen: uint64(t.ID().Len()) + length.Value(),
	}
	n.calcTotalLen()
	return n
}

func (t *NodeTypeDate) NewNode(*Registry) Node {
	return t.New()
}

func (n *DateNode) Type() NodeType {
	return n.typ
}

func (n *DateNode) UnknownLength() bool {
	return false
}

func (n *DateNode) DataLen() uint64 {
	return n.dataLen.Value()
}

func (n *DateNode) TotalLen() uint64 {
	return n.totalLen
}

func (n *DateNode) Value() time.Time {
	return zeroTime.Add(time.Duration(n.value))
}

func (n *DateNode) SetValue(v time.Time) *DateNode {
	n.value = int64(v.Sub(zeroTime))
	n.dataLen = VINTFromUint64(uint64(len(n.bytes())))
	n.calcTotalLen()

	return n
}

func (n *DateNode) WriteTo(writer io.Writer) error {
	err := WriteVINT(writer, n.dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write(n.bytes())
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (n *DateNode) ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error) {
	length, read, err := ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.dataLen = length
	n.calcTotalLen()

	log.Printf("reading date of type: %s (%s) length: %d", n.typ.Name(), n.typ.ID().Hex(), n.dataLen.Value())

	size := length.Value()
	if size == 0 {
		n.value = 0
		return nil, totalRead, nil
	}

	buf := make([]byte, 8)
	read, err = reader.Read(buf[8-size:])
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}

	n.value = bytesToInt64(buf, size)

	return nil, totalRead, nil
}

func (n *DateNode) bytes() []byte {
	if n.value == 0 {
		return nil
	}
	return int64ToBytes(n.value)
}

func (n *DateNode) calcTotalLen() {
	n.totalLen = uint64(n.typ.ID().Len()) + uint64(n.dataLen.Len()) + n.dataLen.Value()
}
