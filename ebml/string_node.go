package ebml

import (
	"io"
	"log"
	"unicode"

	"github.com/pkg/errors"
)

type NodeTypeString struct {
	NodeTypeBase
}

type StringNode struct {
	typ      NodeType
	dataLen  VINT
	totalLen uint64
	value    string
}

func NewNodeTypeString(name string, id VINT) *NodeTypeString {
	return &NodeTypeString{NodeTypeBase: NewNodeTypeBase(name, id)}
}

func (t *NodeTypeString) New() *StringNode {
	length := VINTFromUint64(0)
	n := &StringNode{
		typ:      t,
		dataLen:  length,
		totalLen: uint64(t.ID().Len()) + length.Value(),
	}
	n.calcTotalLen()
	return n
}

func (t *NodeTypeString) NewNode(*Registry) Node {
	return t.New()
}

func (n *StringNode) Type() NodeType {
	return n.typ
}

func (n *StringNode) UnknownLength() bool {
	return false
}

func (n *StringNode) DataLen() uint64 {
	return n.dataLen.Value()
}

func (n *StringNode) TotalLen() uint64 {
	return n.totalLen
}

func (n *StringNode) Value() string {
	return n.value
}

func (n *StringNode) SetValue(v string) *StringNode {
	n.value = v
	n.dataLen = VINTFromUint64(uint64(len(n.value)))
	n.calcTotalLen()

	return n
}

func (n *StringNode) WriteTo(writer io.Writer) error {
	if !isASCII(n.value) {
		return errors.New("string node value may only contain ascii characters")
	}

	err := WriteVINT(writer, n.dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write([]byte(n.value))
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (n *StringNode) ReadFrom(reader io.Reader) (orphanID *VINT, totalRead uint64, err error) {
	length, read, err := ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.dataLen = length
	n.calcTotalLen()

	log.Printf("reading string of type: %s (%s) length: %d", n.typ.Name(), n.typ.ID().Hex(), n.dataLen.Value())
	buf := make([]byte, length.Value())
	read, err = reader.Read(buf)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.value = string(buf)

	return nil, totalRead, nil
}

func isASCII(s string) bool {
	for _, c := range s {
		if c > unicode.MaxASCII {
			return false
		}
	}

	return true
}

func (n *StringNode) calcTotalLen() {
	n.totalLen = uint64(n.typ.ID().Len()) + uint64(n.dataLen.Len()) + n.dataLen.Value()
}
