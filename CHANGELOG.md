# Release Notes

## v1.0.0 / 2020-01-07
- release version v1.0.0
- add verify script and ci job 

## v0.1.2 / 2019-11-06
- fixed a data race for sampling flag in MasterNode
- convert to go modules

## v0.1.1 / 2019-07-02
- fixed a rare race condition causing deadlocks in streaming mode

## v0.1.0 / 2019-06-24
- first release
