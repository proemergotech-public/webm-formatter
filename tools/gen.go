package main

import (
	"encoding/hex"
	"flag"
	"fmt"
	"go/format"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
)

// extraTypes contain type that are marked as not part of webm but are present in the example webm file...
var extraTypes = map[string]bool{
	"SegmentUID":          true,
	"TrackTimestampScale": true,
	"Position":            true,
	"CueRelativePosition": true,
}

var customTypes = map[string]bool{
	"SimpleBlock": true,
}

type node struct {
	group    string
	name     string
	level    int
	id       []byte
	elemType string
	webm     bool
	children []*node
}

func printUsage() {
	fmt.Println("usage: go run gen.go <consts|formatter> <destination_file>")
}

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) != 2 {
		printUsage()
		return
	}

	// Request the HTML page.
	res, err := http.Get("https://matroska.org/technical/specs/index.html")
	if err != nil {
		pan(err)
	}
	defer func() {
		_ = res.Body.Close()
	}()
	if res.StatusCode != 200 {
		pan(errors.Errorf("status code error: %d %s", res.StatusCode, res.Status))
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Panicf("%+v", err)
	}

	root := &node{level: -1}
	parents := []*node{root}
	// Find the review items
	doc.Find("th[colspan='14']").Each(func(i int, s *goquery.Selection) {
		group := s.Text()

		tr := s.Closest("tr")

		//typs += fmt.Sprintf("\n// %s\n", blockName)
		for tr = tr.Next(); tr.Has("th").Length() == 0; tr = tr.Next() {
			tds := tr.Find("td")
			name := strings.TrimSpace(tds.Eq(0).Text())
			levelStr := regexp.MustCompile("[^0-9]").ReplaceAllString(tds.Eq(1).Text(), "")
			level, err := strconv.Atoi(levelStr)
			pan(err)
			id := regexp.MustCompile("[^0-9a-fA-F]").ReplaceAllString(tds.Eq(2).Text(), "")
			elemType := strings.TrimSpace(tds.Eq(7).Text())
			webmSupport := strings.TrimSpace(tds.Eq(12).Text())

			idB := make([]byte, len(id)/2)
			_, err = hex.Decode(idB, []byte(id))
			pan(err)

			n := &node{
				group:    group,
				name:     name,
				level:    level,
				id:       idB,
				elemType: elemType,
				webm:     len(webmSupport) != 0 || extraTypes[name],
			}

			parent := parents[len(parents)-1]
			for parent.level >= level {
				parents = parents[0 : len(parents)-1]
				parent = parents[len(parents)-1]
			}
			parent.children = append(parent.children, n)
			parents = append(parents, n)

			//typs += fmt.Sprintf("Type%s = ebml.NewNodeType(\"%s\", ", name, name)
			//for _, b := range idV.Bytes() {
			//	typs += fmt.Sprintf("0x%s,", hex.EncodeToString([]byte{b}))
			//}
			//typs += ")\n"
		}
	})

	path, err := filepath.Abs(args[1])
	pan(err)

	switch args[0] {
	case "consts":
		f, err := os.Create(path)
		pan(err)

		_, err = f.WriteString(printConst(root))
		pan(err)

	case "formatter":
		f, err := os.Create(path)
		pan(err)

		_, err = f.WriteString(printFormatter(root))
		pan(err)

	default:
		printUsage()
		return
	}
}

func printConst(root *node) string {
	text := `package webm` + "\n\n"
	text += `import "gitlab.com/proemergotech/webm-formatter/ebml"` + "\n\n"
	text += `//go:generate go run ../tools/gen.go consts $GOFILE` + "\n\n"
	text += `var (` + "\n"
	text += root.printConst()
	text += `)` + "\n"

	res, err := format.Source([]byte(text))
	if err != nil {
		return text
	}

	return string(res)
}

func (n *node) printConst() string {
	text := ""
	if n.webm && !customTypes[n.name] {
		idBytes := ""
		for _, b := range n.id {
			idBytes += fmt.Sprintf("0x%s,", hex.EncodeToString([]byte{b}))
		}

		text += fmt.Sprintf("Type%s = ebml.", n.name)
		switch n.elemType {
		case "m":
			text += fmt.Sprintf("NewNodeTypeMaster(\"%s\", ebml.MustVINTFromBytes([]byte{%s}), ", n.name, idBytes)
			for _, c := range n.children {
				if !c.webm {
					continue
				}
				text += fmt.Sprintf("Type%s, ", c.name)
			}

		case "i":
			text += fmt.Sprintf("NewNodeTypeInt(\"%s\", ebml.MustVINTFromBytes([]byte{%s})", n.name, idBytes)

		case "u":
			text += fmt.Sprintf("NewNodeTypeUint(\"%s\", ebml.MustVINTFromBytes([]byte{%s})", n.name, idBytes)

		case "f":
			text += fmt.Sprintf("NewNodeTypeFloat(\"%s\", ebml.MustVINTFromBytes([]byte{%s})", n.name, idBytes)

		case "s":
			text += fmt.Sprintf("NewNodeTypeString(\"%s\", ebml.MustVINTFromBytes([]byte{%s})", n.name, idBytes)

		case "8":
			text += fmt.Sprintf("NewNodeTypeUTF8(\"%s\", ebml.MustVINTFromBytes([]byte{%s})", n.name, idBytes)

		case "d":
			text += fmt.Sprintf("NewNodeTypeDate(\"%s\", ebml.MustVINTFromBytes([]byte{%s})", n.name, idBytes)

		case "b":
			text += fmt.Sprintf("NewNodeTypeBinary(\"%s\", ebml.MustVINTFromBytes([]byte{%s})", n.name, idBytes)
		}

		text += ")\n"
	}

	lastGroup := n.group
	for _, c := range n.children {
		if c.webm && c.group != lastGroup {
			text += fmt.Sprintf("\n// %s\n", c.group)
			lastGroup = c.group
		}

		text += c.printConst()
	}

	return text
}

func printFormatter(root *node) string {
	text := `package webm` + "\n\n"
	text += `import "gitlab.com/proemergotech/webm-formatter/ebml"` + "\n\n"
	text += `//go:generate go run ../tools/gen.go formatter $GOFILE` + "\n\n"
	text += `func addTypes(r *ebml.Registry) {` + "\n"
	text += root.printFormatter()
	text += `}` + "\n"

	res, err := format.Source([]byte(text))
	if err != nil {
		return text
	}

	return string(res)
}

func (n *node) printFormatter() string {
	text := ""
	if n.webm && !customTypes[n.name] {
		text += fmt.Sprintf("r.AddType(Type%s)\n", n.name)
	}

	lastGroup := n.group
	for _, c := range n.children {
		if c.webm && c.group != lastGroup {
			text += fmt.Sprintf("\n// %s\n", c.group)
			lastGroup = c.group
		}

		text += c.printFormatter()
	}

	return text

}

func pan(err error) {
	if err != nil {
		log.Panicf("%+v", errors.WithStack(err))
	}
}
