package main

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/proemergotech/webm-formatter/ebml"
	"gitlab.com/proemergotech/webm-formatter/webm"
)

var (
	videoCodec webm.Codec
	audioCodec webm.Codec

	clusters chan webm.Cluster

	file = "vp9_opus"

	input  = "./data/" + file + ".webm"
	output = "./data/" + file + "_out.webm"
)

func main() {
	err := run()
	if err != nil {
		log.Printf("%+v", err)
		os.Exit(1)
	}
}

func run() error {
	var err error

	err = read()
	if err != nil {
		return err
	}

	err = write()
	if err != nil {
		return err
	}

	return nil
}

func read() error {
	f, err := os.Open(input)
	if err != nil {
		return errors.WithStack(err)
	}

	formatter := webm.NewFormatter()

	root, err := formatter.Parse(f)
	if err != nil {
		return errors.WithStack(err)
	}

	sb := root.FindFirst(webm.TypeSegment, webm.TypeCluster, webm.TypeSimpleBlock)
	fmt.Printf("%#+v\n", sb.(*webm.SimpleBlockNode).Value())

	tracks := root.FindFirst(webm.TypeSegment, webm.TypeTracks).(*ebml.MasterNode)
	for _, t := range tracks.Children() {
		n := t.(*ebml.MasterNode)
		switch n.FindFirst(webm.TypeCodecID).(*ebml.StringNode).Value() {
		case "V_VP8":
			videoCodec, err = webm.VP8CodecFromTrack(n)
			if err != nil {
				return err
			}
		case "V_VP9":
			videoCodec, err = webm.VP9CodecFromTrack(n)
			if err != nil {
				return err
			}

		case "A_VORBIS":
			audioCodec, err = webm.VorbisCodecFromTrack(n)
			if err != nil {
				return err
			}

		case "A_OPUS":
			audioCodec, err = webm.OpusCodecFromTrack(n)
			if err != nil {
				return err
			}
		}
	}

	clusters = make(chan webm.Cluster)

	timestampScale := root.FindFirst(webm.TypeSegment, webm.TypeInfo, webm.TypeTimestampScale).(*ebml.UintNode).Value()

	startTime := time.Now()
	go func() {
		i := 0
		for _, clusterN := range root.FindFirst(webm.TypeSegment).(*ebml.MasterNode).Children() {
			if !clusterN.Type().ID().Equals(webm.TypeCluster.ID()) {
				continue
			}

			clusterNode := clusterN.(*ebml.MasterNode)
			clusterRelTime := clusterNode.FindFirst(webm.TypeTimestamp).(*ebml.UintNode).Value()
			clusterTimestamp := startTime.Add(time.Duration(clusterRelTime * timestampScale))

			frames := make(chan webm.Frame)
			cluster := webm.Cluster{
				Timestamp: clusterTimestamp,
				Frames:    frames,
			}
			clusters <- cluster
			for _, c := range clusterN.(*ebml.MasterNode).Children() {
				n, ok := c.(*webm.SimpleBlockNode)
				if !ok {
					continue
				}

				i++
				if i%100 == 0 {
					log.Printf("written %d packets", i)
					runtime.GC()
					var m runtime.MemStats
					runtime.ReadMemStats(&m)
					fmt.Printf("Mem usage = %v MiB", float64(m.Alloc)/1024/1024)
				}

				block := n.Value()

				timestamp := clusterTimestamp.Add(time.Duration(uint64(block.RelativeTimestamp) * timestampScale))
				wait := timestamp.Sub(time.Now())
				if wait > 0 {
					time.Sleep(wait)
				}

				if block.TrackNumber == 1 {
					frames <- webm.Frame{Video: true, Timestamp: timestamp, Payload: block.Frame}
				} else {
					frames <- webm.Frame{Video: false, Timestamp: timestamp, Payload: block.Frame}
				}
			}
			close(frames)
			clusterN.(*ebml.MasterNode).RemoveChildren()
		}
		close(clusters)
	}()

	return nil
}

func write() error {
	f, err := os.Create(output)
	if err != nil {
		return errors.WithStack(err)
	}

	formatter := webm.NewFormatter()

	err = formatter.WriteSimpleStream(f, videoCodec, audioCodec, clusters)
	if err != nil {
		return err
	}

	return nil
}
