package webm

import (
	"encoding/binary"
	"math/rand"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/proemergotech/webm-formatter/ebml"
)

type opusCodec struct {
	channelCount byte   // 2
	preSkip      uint16 // 312
	sampleRate   uint32 // 48000
	outputGain   uint16 // 0
}

func NewOpusCodec(channelCount byte, preSkip uint16, sampleRate uint32, outputGain uint16) Codec {
	return &opusCodec{channelCount: channelCount, preSkip: preSkip, sampleRate: sampleRate, outputGain: outputGain}
}

func OpusCodecFromTrack(track *ebml.MasterNode) (Codec, error) {
	n := track.FindFirst(TypeCodecPrivate)
	if n == nil {
		return nil, errors.New("invalid track")
	}

	private, ok := n.(*ebml.BinaryNode)
	if !ok {
		return nil, errors.New("invalid track")
	}

	channelCount, preSkip, sampleRate, outputGain, err := parseOpusHead(private.Value())
	if err != nil {
		return nil, err
	}

	return &opusCodec{
		channelCount: channelCount,
		preSkip:      preSkip,
		sampleRate:   sampleRate,
		outputGain:   outputGain,
	}, nil
}

func (c *opusCodec) Track(registry *ebml.Registry, trackNumber uint) (*ebml.MasterNode, error) {
	codecDelay := uint64(time.Duration(c.preSkip) * time.Second / time.Duration(c.sampleRate))

	track := TypeTrackEntry.New(registry)
	track.AddChild(TypeTrackNumber.New().SetValue(uint64(trackNumber)))
	track.AddChild(TypeTrackUID.New().SetValue(rand.Uint64()))
	track.AddChild(TypeTrackType.New().SetValue(2))
	//track.AddChild(TypeTrackTimestampScale.New().SetValue(1))
	track.AddChild(TypeCodecID.New().SetValue("A_OPUS"))
	track.AddChild(TypeCodecName.New().SetValue("Opus"))
	track.AddChild(TypeCodecDelay.New().SetValue(codecDelay))
	track.AddChild(TypeSeekPreRoll.New().SetValue(codecDelay))

	track.AddChild(TypeCodecPrivate.New().SetValue(opusHead(c.channelCount, c.preSkip, c.sampleRate, c.outputGain)))

	audioMeta := TypeAudio.New(registry)
	audioMeta.AddChild(TypeSamplingFrequency.New().SetValue(float64(c.sampleRate)))
	track.AddChild(audioMeta)

	return track, nil
}

func (c *opusCodec) SimpleBlock(trackNumber uint, relativeTimestamp uint, frame []byte) SimpleBlock {
	return SimpleBlock{
		TrackNumber:       trackNumber,
		RelativeTimestamp: relativeTimestamp,
		Frame:             frame,
	}
}

func opusHead(channelCount byte, preSkip uint16, sampleRate uint32, outputGain uint16) []byte {
	b := make([]byte, 19)
	copy(b[0:8], []byte("OpusHead"))
	b[8] = 1 // version
	b[9] = channelCount
	le := binary.LittleEndian
	le.PutUint16(b[10:12], preSkip)
	le.PutUint32(b[12:16], sampleRate)
	le.PutUint16(b[16:18], outputGain)
	b[18] = 0 // mapping family

	return b
}

func parseOpusHead(b []byte) (channelCount byte, preSkip uint16, sampleRate uint32, outputGain uint16, err error) {
	if len(b) != 19 {
		return 0, 0, 0, 0, errors.New("invalid opus header")
	}

	if string(b[0:8]) != "OpusHead" {
		return 0, 0, 0, 0, errors.New("invalid opus header")
	}

	channelCount = b[9]
	le := binary.LittleEndian
	preSkip = le.Uint16(b[10:12])
	sampleRate = le.Uint32(b[12:16])
	outputGain = le.Uint16(b[16:18])

	return channelCount, preSkip, sampleRate, outputGain, nil
}
