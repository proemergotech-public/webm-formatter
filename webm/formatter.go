package webm

import (
	"io"
	"time"

	"gitlab.com/proemergotech/webm-formatter/ebml"
)

var (
	TypeSimpleBlock = NewNodeSimpleBlock("SimpleBlock", ebml.MustVINTFromBytes([]byte{0xa3}))
)

type Formatter struct {
	*ebml.Formatter
}

func NewFormatter() *Formatter {
	r := ebml.NewRegistry()
	addTypes(r)
	r.AddType(TypeSimpleBlock)

	f := ebml.NewFormatter(r)
	f.AddTopLevelType(TypeSegment)

	return &Formatter{
		Formatter: f,
	}
}

func (f *Formatter) WriteHeader(writer io.Writer) error {
	head := ebml.TypeEBML.New(f.Registry())
	head.AddChild(ebml.TypeEBMLVersion.New().SetValue(1))
	head.AddChild(ebml.TypeEBMLReadVersion.New().SetValue(1))
	head.AddChild(ebml.TypeEBMLMaxIDLength.New().SetValue(4))
	head.AddChild(ebml.TypeEBMLMaxSizeLength.New().SetValue(8))
	head.AddChild(ebml.TypeDocType.New().SetValue("webm"))
	head.AddChild(ebml.TypeDocTypeVersion.New().SetValue(1))
	head.AddChild(ebml.TypeDocTypeReadVersion.New().SetValue(1))

	return f.WriteNodes(writer, head)
}

type Frame struct {
	Video     bool
	Timestamp time.Time
	Payload   []byte
}

type Cluster struct {
	Timestamp time.Time
	Frames    <-chan Frame
}

func (f *Formatter) WriteSimpleStream(writer io.Writer, videoCodec Codec, audioCodec Codec, clusters <-chan Cluster) error {
	err := f.WriteHeader(writer)
	if err != nil {
		return err
	}

	segment := TypeSegment.New(f.Registry())

	infoPosition := TypeSeekPosition.New()
	infoVoid := ebml.TypeVoid.New().SetValue(make([]byte, 8))
	tracksPosition := TypeSeekPosition.New()
	tracksVoid := ebml.TypeVoid.New().SetValue(make([]byte, 8))

	seekHead := TypeSeekHead.New(f.Registry())
	infoSeek := TypeSeek.New(f.Registry())
	infoSeek.AddChild(TypeSeekID.New().SetValue(TypeInfo.ID().Bytes()))
	infoSeek.AddChild(infoPosition)
	infoSeek.AddChild(infoVoid)
	seekHead.AddChild(infoSeek)
	tracksSeek := TypeSeek.New(f.Registry())
	tracksSeek.AddChild(TypeSeekID.New().SetValue(TypeTracks.ID().Bytes()))
	tracksSeek.AddChild(tracksPosition)
	tracksSeek.AddChild(tracksVoid)
	seekHead.AddChild(tracksSeek)
	segment.AddChild(seekHead)

	timestampScale := time.Millisecond
	info := TypeInfo.New(f.Registry())
	info.AddChild(TypeTimestampScale.New().SetValue(uint64(time.Millisecond)))
	info.AddChild(TypeDateUTC.New().SetValue(time.Now()))
	info.AddChild(TypeMuxingApp.New().SetValue("gitlab.com/proemergotech/webm-formatter"))
	info.AddChild(TypeWritingApp.New().SetValue("unknown"))
	segment.AddChild(info)

	tracks := TypeTracks.New(f.Registry())

	videoTrack, err := videoCodec.Track(f.Registry(), 1)
	if err != nil {
		return err
	}
	tracks.AddChild(videoTrack)

	audioTrack, err := audioCodec.Track(f.Registry(), 2)
	if err != nil {
		return err
	}
	tracks.AddChild(audioTrack)

	segment.AddChild(tracks)

	infoPosition.SetValue(seekHead.TotalLen())
	infoVoid.SetValue(make([]byte, 8-infoPosition.DataLen()))

	tracksPosition.SetValue(seekHead.TotalLen() + info.TotalLen())
	tracksVoid.SetValue(make([]byte, 8-tracksPosition.DataLen()))

	segment.SetStreaming(true)
	segment.SetDropChildrenAfterWrite(true)

	started := false
	var startTime time.Time

	go func() {
		for c := range clusters {
			if !started {
				startTime = c.Timestamp
				started = true
			}

			cluster := TypeCluster.New(f.Registry())
			cluster.AddChild(TypeTimestamp.New().SetValue(uint64(c.Timestamp.Sub(startTime) / timestampScale)))
			cluster.AddChild(TypePosition.New().SetValue(0))

			cluster.SetStreaming(true)
			cluster.SetDropChildrenAfterWrite(true)

			segment.AddChild(cluster)

			for frame := range c.Frames {
				if frame.Video {
					cluster.AddChild(TypeSimpleBlock.New().SetValue(videoCodec.SimpleBlock(1, uint(frame.Timestamp.Sub(c.Timestamp)/timestampScale), frame.Payload)))
				} else {
					cluster.AddChild(TypeSimpleBlock.New().SetValue(audioCodec.SimpleBlock(2, uint(frame.Timestamp.Sub(c.Timestamp)/timestampScale), frame.Payload)))
				}
			}

			cluster.SetStreaming(false)
		}

		segment.SetStreaming(false)
	}()

	return f.WriteNodes(writer, segment)
}
