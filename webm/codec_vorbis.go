package webm

import (
	"encoding/binary"
	"math/rand"

	"github.com/pkg/errors"
	"gitlab.com/proemergotech/webm-formatter/ebml"
)

type vorbisCodec struct {
	header     VorbisHeader
	sampleRate uint32
}

type VorbisHeader struct {
	Indent  VorbisIdentificationHeader
	Comment VorbisCommentHeader
	Setup   VorbisSetupHeader
}

type VorbisIdentificationHeader struct {
	Version        uint32
	SampleRate     uint32
	Channels       byte
	BitrateNominal uint32
	BitrateMax     uint32
	BitrateMin     uint32
	BlockSize      byte
}

// The CommentHeader of a vorbis stream.
type VorbisCommentHeader struct {
	Vendor   string
	Comments []string
}

type VorbisSetupHeader struct {
	Content []byte
}

func NewVorbisCodec(header VorbisHeader, sampleRate uint32) Codec {
	return &vorbisCodec{
		header:     header,
		sampleRate: sampleRate,
	}
}

func VorbisCodecFromTrack(track *ebml.MasterNode) (Codec, error) {
	n := track.FindFirst(TypeCodecPrivate)
	if n == nil {
		return nil, errors.New("invalid track")
	}

	private, ok := n.(*ebml.BinaryNode)
	if !ok {
		return nil, errors.New("invalid track")
	}

	header, err := readVorbisHeaders(private.Value())
	if err != nil {
		return nil, errors.WithStack(err)
	}

	n = track.FindFirst(TypeAudio, TypeSamplingFrequency)
	if n == nil {
		return nil, errors.New("invalid track")
	}
	sampleRate, ok := n.(*ebml.FloatNode)
	if !ok {
		return nil, errors.New("invalid track")
	}

	return &vorbisCodec{
		header:     header,
		sampleRate: uint32(sampleRate.Value()),
	}, nil
}

func (c *vorbisCodec) Track(registry *ebml.Registry, trackNumber uint) (*ebml.MasterNode, error) {
	track := TypeTrackEntry.New(registry)
	track.AddChild(TypeTrackNumber.New().SetValue(uint64(trackNumber)))
	track.AddChild(TypeTrackUID.New().SetValue(rand.Uint64()))
	track.AddChild(TypeTrackType.New().SetValue(2))
	track.AddChild(TypeTrackTimestampScale.New().SetValue(1))
	track.AddChild(TypeCodecID.New().SetValue("A_VORBIS"))
	track.AddChild(TypeCodecName.New().SetValue("Vorbis"))

	private, err := c.header.Bytes()
	if err != nil {
		return nil, err
	}
	track.AddChild(TypeCodecPrivate.New().SetValue(private))

	audioMeta := TypeAudio.New(registry)
	audioMeta.AddChild(TypeSamplingFrequency.New().SetValue(float64(c.sampleRate)))
	track.AddChild(audioMeta)

	return track, nil
}

func (c *vorbisCodec) SimpleBlock(trackNumber uint, relativeTimestamp uint, frame []byte) SimpleBlock {
	return SimpleBlock{
		TrackNumber:       trackNumber,
		RelativeTimestamp: relativeTimestamp,
		Frame:             frame,
	}
}

func readVorbisIdentificationHeader(b []byte) (VorbisIdentificationHeader, error) {
	h := VorbisIdentificationHeader{}

	if len(b) != 30 {
		return VorbisIdentificationHeader{}, errors.New("vorbis: decoding error")
	}
	le := binary.LittleEndian

	headerType := b[0]
	if headerType != 1 {
		return VorbisIdentificationHeader{}, errors.New("vorbis: decoding error")
	}

	vorbisStr := b[1:7]
	if string(vorbisStr) != "vorbis" {
		return VorbisIdentificationHeader{}, errors.New("vorbis: decoding error")
	}

	h.Version = le.Uint32(b[7:11])
	h.Channels = b[11]
	h.SampleRate = le.Uint32(b[12:16])
	h.BitrateMax = le.Uint32(b[16:20])
	h.BitrateNominal = le.Uint32(b[20:24])
	h.BitrateMin = le.Uint32(b[24:28])
	h.BlockSize = b[28]
	if b[29]&1 == 0 {
		return VorbisIdentificationHeader{}, errors.New("vorbis: decoding error")
	}

	return h, nil
}

func (h VorbisIdentificationHeader) Bytes() ([]byte, error) {
	b := make([]byte, 30)

	b[0] = 1
	copy(b[1:7], []byte("vorbis"))

	le := binary.LittleEndian
	le.PutUint32(b[7:11], h.Version)
	b[11] = h.Channels
	le.PutUint32(b[12:16], h.SampleRate)
	le.PutUint32(b[16:20], h.BitrateMax)
	le.PutUint32(b[20:24], h.BitrateNominal)
	le.PutUint32(b[24:28], h.BitrateMin)
	b[28] = h.BlockSize
	b[29] = 1

	return b, nil
}

func readVorbisCommentHeader(b []byte) (VorbisCommentHeader, error) {
	h := VorbisCommentHeader{}

	headerType := b[0]
	if headerType != 3 {
		return VorbisCommentHeader{}, errors.New("vorbis: decoding error")
	}

	vorbisStr := b[1:7]
	if string(vorbisStr) != "vorbis" {
		return VorbisCommentHeader{}, errors.New("vorbis: decoding error")
	}
	b = b[7:]

	le := binary.LittleEndian
	vendorLen := le.Uint32(b[0:4])
	b = b[4:]
	if uint32(len(b)) < vendorLen {
		return VorbisCommentHeader{}, errors.New("vorbis: decoding error")
	}

	h.Vendor = string(b[:vendorLen])
	b = b[vendorLen:]
	numComments := int(le.Uint32(b[0:4]))
	h.Comments = make([]string, numComments)
	b = b[4:]
	for i := 0; i < numComments; i++ {
		commentLen := le.Uint32(b[0:4])
		b = b[4:]
		if uint32(len(b)) < commentLen {
			return VorbisCommentHeader{}, errors.New("vorbis: decoding error")
		}

		h.Comments[i] = string(b[:commentLen])
		b = b[commentLen:]
	}

	return h, nil
}

func (h VorbisCommentHeader) Bytes() ([]byte, error) {
	le := binary.LittleEndian

	length := 7 + len(h.Vendor) + 4 + 4 + 1
	for _, c := range h.Comments {
		length += len(c) + 4
	}

	fullB := make([]byte, length)
	b := fullB
	b[0] = 3
	copy(b[1:7], []byte("vorbis"))
	b = b[7:]

	le.PutUint32(b[0:4], uint32(len(h.Vendor)))
	b = b[4:]
	copy(b, []byte(h.Vendor))
	b = b[len(h.Vendor):]
	le.PutUint32(b[0:4], uint32(len(h.Comments)))
	b = b[4:]
	for _, c := range h.Comments {
		le.PutUint32(b[0:4], uint32(len(c)))
		b = b[4:]
		copy(b, []byte(c))
		b = b[len(c):]
	}
	b[0] = 1

	return fullB, nil
}

func readVorbisSetupHeader(b []byte) (VorbisSetupHeader, error) {
	headerType := b[0]
	if headerType != 5 {
		return VorbisSetupHeader{}, errors.New("vorbis: decoding error")
	}

	vorbisStr := b[1:7]
	if string(vorbisStr) != "vorbis" {
		return VorbisSetupHeader{}, errors.New("vorbis: decoding error")
	}
	b = b[7:]

	return VorbisSetupHeader{Content: b}, nil
}

func (h VorbisSetupHeader) Bytes() ([]byte, error) {
	b := make([]byte, len(h.Content)+7)
	b[0] = 5
	copy(b[1:7], []byte("vorbis"))

	copy(b[7:], h.Content)

	return b, nil
}

func readVorbisHeaders(b []byte) (VorbisHeader, error) {
	numFrames := b[0]
	if numFrames != 2 {
		return VorbisHeader{}, errors.New("invalid vorbis header")
	}
	b = b[1:]

	i := 0
	for ; b[i] == 255; i++ {
	}
	identLength := i*255 + int(b[i])
	b = b[i+1:]

	i = 0
	for ; b[i] == 255; i++ {
	}
	commentLength := i*255 + int(b[i])
	b = b[i+1:]

	h := VorbisHeader{}
	var err error
	h.Indent, err = readVorbisIdentificationHeader(b[:identLength])
	if err != nil {
		return VorbisHeader{}, err
	}
	b = b[identLength:]

	h.Comment, err = readVorbisCommentHeader(b[:commentLength])
	if err != nil {
		return VorbisHeader{}, err
	}
	b = b[commentLength:]

	h.Setup, err = readVorbisSetupHeader(b)
	if err != nil {
		return VorbisHeader{}, err
	}

	return h, nil
}

func (h VorbisHeader) Bytes() ([]byte, error) {
	indent, err := h.Indent.Bytes()
	if err != nil {
		return nil, err
	}
	indentLength := len(indent)

	comment, err := h.Comment.Bytes()
	if err != nil {
		return nil, err
	}
	commentLength := len(comment)

	setup, err := h.Setup.Bytes()
	if err != nil {
		return nil, err
	}
	setupLength := len(setup)

	fullB := make([]byte, 1+
		indentLength/255+1+indentLength+
		commentLength/255+1+commentLength+
		setupLength)
	b := fullB
	b[0] = 2
	b = b[1:]

	i := 0
	for ; i < indentLength/255; i++ {
		b[i] = 255
	}
	b[i] = byte(indentLength % 255)
	b = b[i+1:]

	i = 0
	for ; i < commentLength/255; i++ {
		b[i] = 255
	}
	b[i] = byte(commentLength % 255)
	b = b[i+1:]

	copy(b[:indentLength], indent)
	b = b[indentLength:]
	copy(b[:commentLength], comment)
	b = b[commentLength:]
	copy(b[:setupLength], setup)

	return fullB, nil
}
