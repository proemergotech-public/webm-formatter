package webm

import (
	"math/rand"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/proemergotech/webm-formatter/ebml"
)

type vp9Codec struct {
	width                uint
	height               uint
	interlaced           bool
	progressive          bool
	defaultFrameDuration time.Duration
}

func NewVP9Codec(width uint, height uint, interlaced bool, progressive bool) Codec {
	return &vp9Codec{width: width, height: height, interlaced: interlaced, progressive: progressive}
}

func VP9CodecFromTrack(track *ebml.MasterNode) (Codec, error) {
	n := track.FindFirst(TypeVideo, TypePixelWidth)
	if n == nil {
		return nil, errors.New("invalid track")
	}

	width, ok := n.(*ebml.UintNode)
	if !ok {
		return nil, errors.New("invalid track")
	}

	n = track.FindFirst(TypeVideo, TypePixelHeight)
	if n == nil {
		return nil, errors.New("invalid track")
	}

	height, ok := n.(*ebml.UintNode)
	if !ok {
		return nil, errors.New("invalid track")
	}

	n = track.FindFirst(TypeDefaultDuration)
	if n == nil {
		return nil, errors.New("invalid track")
	}

	defaultDuration, ok := n.(*ebml.UintNode)
	if !ok {
		return nil, errors.New("invalid track")
	}
	codec := &vp9Codec{
		width:                uint(width.Value()),
		height:               uint(height.Value()),
		defaultFrameDuration: time.Duration(defaultDuration.Value()),
	}

	n = track.FindFirst(TypeFlagInterlaced)
	if n != nil {
		interlaced, ok := n.(*ebml.UintNode)
		if !ok {
			return nil, errors.New("invalid track")
		}

		codec.interlaced = interlaced.Value() == 1
		codec.progressive = interlaced.Value() == 2
	}

	return codec, nil
}

func (c *vp9Codec) Track(registry *ebml.Registry, trackNumber uint) (*ebml.MasterNode, error) {
	track := TypeTrackEntry.New(registry)
	track.AddChild(TypeTrackNumber.New().SetValue(uint64(trackNumber)))
	track.AddChild(TypeTrackUID.New().SetValue(rand.Uint64()))
	track.AddChild(TypeTrackType.New().SetValue(1))
	if c.defaultFrameDuration != 0 {
		track.AddChild(TypeDefaultDuration.New().SetValue(uint64(c.defaultFrameDuration)))
	}
	track.AddChild(TypeTrackTimestampScale.New().SetValue(1))
	track.AddChild(TypeCodecID.New().SetValue("V_VP9"))
	track.AddChild(TypeCodecName.New().SetValue("VP9"))

	meta := TypeVideo.New(registry)
	meta.AddChild(TypePixelWidth.New().SetValue(uint64(c.width)))
	meta.AddChild(TypePixelHeight.New().SetValue(uint64(c.height)))
	if c.progressive {
		meta.AddChild(TypeFlagInterlaced.New().SetValue(2))
	} else if c.interlaced {
		meta.AddChild(TypeFlagInterlaced.New().SetValue(1))
	}

	track.AddChild(meta)

	return track, nil
}

func (c *vp9Codec) SimpleBlock(trackNumber uint, relativeTimestamp uint, frame []byte) SimpleBlock {
	return SimpleBlock{
		TrackNumber:       trackNumber,
		RelativeTimestamp: relativeTimestamp,
		KeyFrame:          (frame[0] & 0x1) == 0,
		Frame:             frame,
	}
}
