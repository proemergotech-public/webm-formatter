package webm

import (
	"math/rand"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/proemergotech/webm-formatter/ebml"
)

type vp8Codec struct {
	width                uint
	height               uint
	defaultFrameDuration time.Duration
}

func NewVP8Codec(width uint, height uint) Codec {
	return &vp8Codec{width: width, height: height}
}

func VP8CodecFromTrack(track *ebml.MasterNode) (Codec, error) {
	n := track.FindFirst(TypeVideo, TypePixelWidth)
	if n == nil {
		return nil, errors.New("invalid track")
	}

	width, ok := n.(*ebml.UintNode)
	if !ok {
		return nil, errors.New("invalid track")
	}

	n = track.FindFirst(TypeVideo, TypePixelHeight)
	if n == nil {
		return nil, errors.New("invalid track")
	}

	height, ok := n.(*ebml.UintNode)
	if !ok {
		return nil, errors.New("invalid track")
	}

	n = track.FindFirst(TypeDefaultDuration)
	if n == nil {
		return nil, errors.New("invalid track")
	}

	defaultDuration, ok := n.(*ebml.UintNode)
	if !ok {
		return nil, errors.New("invalid track")
	}

	return &vp8Codec{
		width:                uint(width.Value()),
		height:               uint(height.Value()),
		defaultFrameDuration: time.Duration(defaultDuration.Value()),
	}, nil
}

func (c *vp8Codec) Track(registry *ebml.Registry, trackNumber uint) (*ebml.MasterNode, error) {
	track := TypeTrackEntry.New(registry)
	track.AddChild(TypeTrackNumber.New().SetValue(uint64(trackNumber)))
	track.AddChild(TypeTrackUID.New().SetValue(rand.Uint64()))
	track.AddChild(TypeTrackType.New().SetValue(1))
	if c.defaultFrameDuration != 0 {
		track.AddChild(TypeDefaultDuration.New().SetValue(uint64(c.defaultFrameDuration)))
	}
	track.AddChild(TypeTrackTimestampScale.New().SetValue(1))
	track.AddChild(TypeCodecID.New().SetValue("V_VP8"))
	track.AddChild(TypeCodecName.New().SetValue("VP8"))

	meta := TypeVideo.New(registry)
	meta.AddChild(TypePixelWidth.New().SetValue(uint64(c.width)))
	meta.AddChild(TypePixelHeight.New().SetValue(uint64(c.height)))
	track.AddChild(meta)

	return track, nil
}

func (c *vp8Codec) SimpleBlock(trackNumber uint, relativeTimestamp uint, frame []byte) SimpleBlock {
	return SimpleBlock{
		TrackNumber:       trackNumber,
		RelativeTimestamp: relativeTimestamp,
		KeyFrame:          (frame[0] & 0x1) == 0,
		Frame:             frame,
	}
}
