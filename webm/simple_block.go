package webm

import (
	"io"
	"log"

	"github.com/pkg/errors"
	"gitlab.com/proemergotech/webm-formatter/ebml"
)

type NodeTypeSimpleBlock struct {
	ebml.NodeTypeBase
}

type SimpleBlockNode struct {
	dataLen  ebml.VINT
	totalLen uint64
	value    SimpleBlock
}

type SimpleBlock struct {
	TrackNumber       uint
	RelativeTimestamp uint
	KeyFrame          bool
	LacingXiph        bool
	LacingEBML        bool
	LacingFixed       bool
	Discardable       bool
	Frame             []byte
}

func NewNodeSimpleBlock(name string, id ebml.VINT) *NodeTypeSimpleBlock {
	return &NodeTypeSimpleBlock{NodeTypeBase: ebml.NewNodeTypeBase(name, id)}
}

func (t *NodeTypeSimpleBlock) New() *SimpleBlockNode {
	length := ebml.VINTFromUint64(0)
	n := &SimpleBlockNode{
		dataLen:  length,
		totalLen: uint64(t.ID().Len()) + length.Value(),
	}
	n.calcTotalLen()
	return n
}

func (t *NodeTypeSimpleBlock) NewNode(*ebml.Registry) ebml.Node {
	return t.New()
}

func (n *SimpleBlockNode) Type() ebml.NodeType {
	return TypeSimpleBlock
}

func (n *SimpleBlockNode) UnknownLength() bool {
	return false
}

func (n *SimpleBlockNode) DataLen() uint64 {
	return n.dataLen.Value()
}

func (n *SimpleBlockNode) TotalLen() uint64 {
	return n.totalLen
}

func (n *SimpleBlockNode) Value() SimpleBlock {
	return n.value
}

func (n *SimpleBlockNode) SetValue(v SimpleBlock) *SimpleBlockNode {
	n.value = v
	n.dataLen = ebml.VINTFromUint64(uint64(len(n.header()) + len(n.value.Frame)))
	n.calcTotalLen()

	return n
}

func (n *SimpleBlockNode) ReadFrom(reader io.Reader) (orphanID *ebml.VINT, totalRead uint64, err error) {
	length, read, err := ebml.ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	n.dataLen = length
	n.calcTotalLen()

	log.Printf("reading simpleblock of type: %s (%s) length: %d", n.Type().Name(), n.Type().ID().Hex(), n.dataLen.Value())

	trackNumber, read, err := ebml.ReadVINT(reader)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}

	timestamp := make([]byte, 2)
	read, err = reader.Read(timestamp)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}
	flags := make([]byte, 1)
	read, err = reader.Read(flags)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}

	frame := make([]byte, length.Value()-uint64(trackNumber.Len()+3))
	read, err = reader.Read(frame)
	totalRead += uint64(read)
	if err != nil {
		return nil, totalRead, err
	}

	lacing := flags[0] & 0x06
	n.value = SimpleBlock{
		TrackNumber:       uint(trackNumber.Value()),
		RelativeTimestamp: (uint(timestamp[0]) << 8) + uint(timestamp[1]),
		KeyFrame:          (flags[0] & 0x80) != 0,
		LacingXiph:        lacing == 0x02,
		LacingEBML:        lacing == 0x06,
		LacingFixed:       lacing == 0x04,
		Discardable:       (flags[0] & 0x01) != 0,
		Frame:             frame,
	}

	return nil, totalRead, nil
}

func (n *SimpleBlockNode) WriteTo(writer io.Writer) error {
	err := ebml.WriteVINT(writer, n.dataLen)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write(n.header())
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = writer.Write(n.value.Frame)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (n *SimpleBlockNode) header() []byte {
	trackNumber := ebml.VINTFromUint64(uint64(n.value.TrackNumber))

	header := make([]byte, 0, trackNumber.Len()+3)
	header = append(header, trackNumber.Bytes()...)
	header = append(header, byte(n.value.RelativeTimestamp>>8), byte(n.value.RelativeTimestamp))

	var flags byte
	if n.value.KeyFrame {
		flags |= 0x80
	}
	if n.value.LacingXiph {
		flags |= 0x02
	} else if n.value.LacingEBML {
		flags |= 0x06
	} else if n.value.LacingFixed {
		flags |= 0x04
	}
	if n.value.Discardable {
		flags |= 0x01
	}
	header = append(header, flags)

	return header
}

func (n *SimpleBlockNode) calcTotalLen() {
	n.totalLen = uint64(n.Type().ID().Len()) + uint64(n.dataLen.Len()) + n.dataLen.Value()
}
