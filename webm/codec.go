package webm

import (
	"gitlab.com/proemergotech/webm-formatter/ebml"
)

type Codec interface {
	Track(registry *ebml.Registry, trackNumber uint) (*ebml.MasterNode, error)
	SimpleBlock(trackNumber uint, relativeTimestamp uint, frame []byte) SimpleBlock
}
